module main(
	input wire reset,
	input wire clk,
	
	input wire button0,
	input wire button1,
	input wire button2,
	
	input wire sw1,
	
	output wire [6:0] hex0
); 
	
	//constants
	parameter SECOND = 50_000_000;
	parameter HALF_SECOND = SECOND / 2;
	
	localparam DISP_OFF = 7'h7F;
	
	localparam INITIAL_STATE = 4'h0;
	localparam HISTERESIS_STATE = 4'h1;
	localparam NORMAL_STATE = 4'h2;
	localparam DISPLAY_STATE = 4'h3;
	
	//registers
	integer digits_next, digits_reg;
	integer timer_next, timer_reg;
	
	reg [3:0] state_next, state_reg;
	
	reg [7:0] received_data_next, received_data_reg;
	reg[4:0] size_of_received_data_next, size_of_received_data_reg;
	
	reg [4:0] index_of_disp_value_reg, index_of_disp_value_next;
	//display
	reg [3:0] disp0;
	//rising edge detection
	wire red_b0, red_b1, red_b2;
	/////////////////////////////////////
	red r0(
		.clk(clk),
		.reset(reset),
		.input_value(button0),
		.output_value(red_b0)
	);
	red r1(
		.clk(clk),
		.reset(reset),
		.input_value(button1),
		.output_value(red_b1)
	);
	red r2(
		.clk(clk),
		.reset(reset),
		.input_value(button2),
		.output_value(red_b2)
	);
	/////////////////////////////////////
	bin_to_ss bin0(
		.input_value(disp0),
		.output_value(hex0)
	);
	/////////////////////////////////////
	
	
	always @ (posedge clk, posedge reset) begin
		if(reset) begin
			digits_reg <= 0;
			state_reg <= INITIAL_STATE;
			
			size_of_received_data_reg <= 0;
			received_data_reg <= 0;
			timer_reg <= 0;
			index_of_disp_value_reg <= 0;
		end else begin
			digits_reg <= digits_next;
			state_reg <= state_next;
			
			size_of_received_data_reg <= size_of_received_data_next;
			received_data_reg <= received_data_next;
			timer_reg <= timer_next;
			index_of_disp_value_reg <= index_of_disp_value_next;
		end
	end
	
	
	always @ (*) begin
		digits_next = digits_reg;
		state_next = state_reg;
		
		disp0 = DISP_OFF;
		
		size_of_received_data_next = size_of_received_data_reg;
		received_data_next = received_data_reg;
		timer_next = timer_reg;
		index_of_disp_value_next = index_of_disp_value_reg;
		
		
		case(state_reg)
			INITIAL_STATE:
				begin
					
					if(red_b0) begin
						digits_next = digits_reg - 1;
					end else if(red_b1) begin
						digits_next = digits_reg + 1;
					end
					
					if(red_b0 || red_b1) begin
						received_data_next = {received_data_reg[6:0], red_b1};
						size_of_received_data_next = 1;
					end
					
					//next state
					if(red_b2) begin
						state_next = HISTERESIS_STATE;
					end else if(red_b0 || red_b1) begin
						state_next = NORMAL_STATE;
					end
				end	//INITIAL_STATE
		
			NORMAL_STATE:
				begin
				
					if(red_b0) begin
						digits_next = digits_reg - 1;
					end else if(red_b1) begin
						digits_next = digits_reg + 1;
					end
					
					
					if(red_b0 || red_b1) begin
						received_data_next = {received_data_reg[6:0], red_b1};
						size_of_received_data_next = (size_of_received_data_reg == 4'h8)? 8 : size_of_received_data_reg + 1;
					end
					
					if(digits_reg > 0) begin
						disp0 = 1;
					end else if(digits_reg < 0) begin
						disp0 = 0;
					end
					
					//next state
					if(sw1) begin
						timer_next = 0;
						index_of_disp_value_next = size_of_received_data_reg;
						
						state_next = DISPLAY_STATE;
					end
					
				end	//NORMAL_STATE
				
			HISTERESIS_STATE:
				begin
				
					if(red_b0) begin
						digits_next = digits_reg - 1;
					end else if(red_b1) begin
						digits_next = digits_reg + 1;
					end
					
					if(red_b0 || red_b1) begin
						received_data_next = {received_data_reg[6:0], red_b1};
						size_of_received_data_next = (size_of_received_data_reg == 4'h8)? 8 : size_of_received_data_reg + 1;
					end
					
					if(digits_reg > 1) begin
						disp0 = 1;
					end else if(digits_reg < -1) begin
						disp0 = 0;
					end
					
					//next state
					if(sw1) begin
						timer_next = 0;
						index_of_disp_value_next = size_of_received_data_reg;
						state_next = DISPLAY_STATE;
					end
				
				end	//HISTERESIS_STATE
				
			DISPLAY_STATE:
				begin
					timer_next = (timer_reg == SECOND)? 0: timer_reg + 1;
					
					if(timer_reg < HALF_SECOND) begin
						disp0 = DISP_OFF;
					end else begin
						disp0 = (received_data_reg[index_of_disp_value_reg - 1] == 1'b1)? 4'h1 : 4'h0;
					end
					
					if(timer_reg == SECOND) begin
						index_of_disp_value_next = (index_of_disp_value_reg == 0)? 0: index_of_disp_value_reg - 1;
					end
				
					if(index_of_disp_value_reg == 0) begin
						state_next = INITIAL_STATE;
					end
				end	//DISPLAY_STATE
					
		endcase
	end
				
endmodule












