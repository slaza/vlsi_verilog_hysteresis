//`timescale 10ns / 10ns

module test();


	reg clk, reset, button0, button1, button2, sw1;
	wire [6:0] hex0;
	
	defparam m0.SECOND = 9;
	main m0(
		.clk(clk),
		.reset(reset),
		.button0(button0),
		.button1(button1),
		.button2(button2),
		.hex0(hex0),
		
		.sw1(sw1)
	);

	initial begin
		$dumpfile("dump.vcd");
		$dumpvars(0, test);
	
	
		clk = 0;
		reset = 0;
		button0 = 0;
		button1 = 0;
		button2 = 0;
		sw1 = 0;
	end

	always begin
		#1 clk = ~clk;
	end
	
	initial begin
	
		#4 reset = 1;
		#4 reset = 0;
		
		//NORMAL STATE
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 button1 = 1;
		#4 button1 = 0;
		
		#4 button1 = 1;
		#4 button1 = 0;
		
		#4 button1 = 1;
		#4 button1 = 0;
		
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 button0 = 1;
		#4 button0 = 0;
		
		
		//DISPLAY STATE
		#4 sw1 = 1;
		#4 sw1 = 0;
		
		
		//HYSTERESIS STATE
		#150 reset = 1;
		#4 reset = 0;
		
		#4 button2 = 1;
		#4 button2 = 0;
		
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 button1 = 1;
		#4 button1 = 0;
		
		#4 button1 = 1;
		#4 button1 = 0;
		
		#4 button1 = 1;
		#4 button1 = 0;
		
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 button0 = 1;
		#4 button0 = 0;
		
		#4 button0 = 1;
		#4 button0 = 0;
		
		//DISPLAY STATE
		#4 sw1 = 1;
		#4 sw1 = 0;
		
		#300 $finish;
	end
	
endmodule
